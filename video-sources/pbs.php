<?php

/**
 * Checks user input from the shortcode and returns the video embed code
 *
 * @param string $vidID the video URL or legacy ID
 * @param string $width the width of the video which is an int but treated as a string for concatenation
 * @param string $height the height of the video which is an int but treated as a string for concatenation
 *
 * @return string Returns errors or the embed code
 **/
function wunrav_get_pbsvid( $vidID, $width, $height = NULL ){

    // set/get the dimensions for the iframe
    if ( ! $height && $width != 1400 ){
	$height = $width * 0.562;
    } else {
	$height = 788;
    }

    if ( ! isset($vidID) ) {
        $err = '<h1 style="color:#f00;">Oops! The [pbsvid] shortcode was used but no video URL or ID was provided.</h1>';
        return $err;
    } elseif ( preg_match( '/[0-9]{10}?/', $vidID, $result ) ){ // Legacy ID. API not used.
        $id = $result[0];
        $legacy = true;
    } elseif ( preg_match( '/\/video\/([a-z0-9\-]+)/', $vidID, $result) ) { // media manager asset slug
        if ( ! is_plugin_active('pbs-media-manager-wordpress') ){
            $err = '<h1 style="color:#f00;">You need the <a target="_blank" rel="noopner" href="https://github.com/tamw-wnet/pbs-media-manager-wordpress">PBS Media Manager WordPress plugin</a> to use a full URL. There is a <a target="_blank" rel="noopener" href="https://gitlab.com/webunraveling/wp-video-embed-shortcode#requirements">reason why</a> the additional plugin is required for this.</h1>';
            return $err;
        }

        $id = $result[1];
    }
    
    $iframe  = '<div>'; // adding an extra wrapper in case a theme uses calc() to adjust width
    $iframe .= '<div class="wunrav-video-wrapper">';
    $iframe .= sprintf('<iframe scrolling="no" src="https://player.pbs.org/%s/%s" frameborder="0" marginwidth="0" marginheight="0" width="100%%" height="100%%" allowfullscreen></iframe>',
        (isset($legacy) ? 'portalplayer': 'partnerplayer'), $id );
    $iframe .= '</div>';
    $iframe .= '</div>';

    if ( isset($err) && current_user_can('edit_posts') ) { echo $err; }
    if ( isset($iframe) ){ return $iframe; }

}

/**
 * Grabs attributes from the shortcode
 *
 * @param array $atts the attributes from the shortcode
 * @param string $content the URL or ID of the video
 *
 * @return the embedded video with formatting based on $atts
 **/
add_shortcode('pbsvid', 'wunrav_pbsvid_shortcode');
function wunrav_pbsvid_shortcode( $atts, $content = null ){

    extract( shortcode_atts(array("height" => '', "width" => '1400', "align" => 'aligncenter'), $atts) );

    // set alignment in the shortcode with align="alignleft"
    if ( $align == 'alignleft' || $align == 'left' ) {
        $align = "alignleft";
    } elseif ( $align == 'alignright' || $align == 'right' ) {
        $align = "alignright";
    } else {
        $align = "aligncenter";
    }
    
    $pbsvid = wunrav_get_pbsvid($content, $width, $height);

    return $pbsvid;
}
